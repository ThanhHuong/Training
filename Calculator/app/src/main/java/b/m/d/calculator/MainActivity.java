package b.m.d.calculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Arrays;
import java.util.Stack;

class InfixToPostfix{
    boolean check_error = false;

    public boolean isOperator(char c){ 	// kiem tra xem co phai toan tu
        char operator[] = { '+', '-', '×', '÷'};
        Arrays.sort(operator);
        if (Arrays.binarySearch(operator, c) > -1)
            return true;
        else return false;
    }

    public String standardizeDouble(double num){ //chuan hoa so
        int a = (int)num;
        if (a == num)
            return Integer.toString(a);
        else return Double.toString(num);
    }

    public int priority(char c){		// thiet lap thu tu uu tien
        switch (c) {
            case '+' : case '-' : return 1;
            case '×' : case '÷' : return 2;
        }
        return 0;
    }

    public String[] processString(String sMath){ // xu ly bieu thuc nhap vao thanh cac phan tu
        String s1 = "", elementMath[] = null;
        InfixToPostfix  ITP = new InfixToPostfix();
        for (int i=0; i<sMath.length(); i++){
            char c = sMath.charAt(i);
            if (!ITP.isOperator(c))
                s1 = s1 + c;
            else
                s1 = s1 + " " + c + " ";
        }
        s1 = s1.trim();
        s1 = s1.replaceAll("\\s+"," "); //	chuan hoa s1
        elementMath = s1.split(" "); //tach s1 thanh cac phan tu
        return elementMath;
    }

    public String[] postfix(String[] elementMath){
        InfixToPostfix  ITP = new InfixToPostfix();
        String s1 = "", E[];
        Stack <String> S = new Stack<String>();
        for (int i=0; i<elementMath.length; i++){ 	// duyet cac phan tu
            char c = elementMath[i].charAt(0);		// c la ky tu dau tien cua moi phan tu

            if (!ITP.isOperator(c)) 				// neu c khong la toan tu
                s1 = s1 + elementMath[i] + " ";		// xuat elem vao s1
            else{									// c la toan tu

                while (!S.isEmpty() && ITP.priority(S.peek().charAt(0)) >= ITP.priority(c))
                    s1 = s1 + S.pop() + " ";
                S.push(elementMath[i]); // 	dua phan tu hien tai vao Stack
            }
        }
        while (!S.isEmpty()) s1 = s1 + S.pop() + " "; // Neu Stack con phan tu thi day het vao s1
        E = s1.split(" ");	//	tach s1 thanh cac phan tu
        return E;
    }

    public String valueMath(String[] elementMath){
        Stack<Double> S = new Stack<Double>();
        InfixToPostfix  ITP = new InfixToPostfix();
        double num = 0.0;
        for (int i=0; i<elementMath.length; i++){
            char c = elementMath[i].charAt(0);
            if (!ITP.isOperator(c))
                S.push(Double.parseDouble(elementMath[i])); //so
            else{	// toan tu
                double num1 = S.pop();

                if (!S.empty()){
                    double num2 = S.peek();
                    switch (c) {
                        //-----------------------
                        case '+' : num = num2 + num1; S.pop(); break;
                        case '-' : num = num2 - num1; S.pop(); break;
                        case '×' : num = num2 * num1; S.pop(); break;
                        case '÷' : {
                            if (num1 != 0) num = num2 / num1;
                            else check_error = true;
                            S.pop(); break;
                        }
                        case '^' : num = Math.pow(num2, num1); S.pop(); break;
                    }
                }
                S.push(num);
            }
        }
        return standardizeDouble(S.pop());
    }
}

public class MainActivity extends AppCompatActivity {
    EditText edt1, edt2;
    Button btn0,btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btnDot,btnPlus,btnMinus,btnMultiply,btnDivide,btnEqual;
    Button btnClear, btnBackspace;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edt1 = (EditText) findViewById(R.id.edt1);
        edt2 = (EditText) findViewById(R.id.edt2);

        btn0 = (Button) findViewById(R.id.btn0);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btnDot = (Button) findViewById(R.id.btnDot);
        btnPlus = (Button) findViewById(R.id.btnPlus);
        btnMinus = (Button) findViewById(R.id.btnMinus);
        btnMultiply = (Button) findViewById(R.id.btnMultiply);
        btnDivide = (Button) findViewById(R.id.btnDivide);
        btnEqual = (Button) findViewById(R.id.btnEqual);
        btnClear = (Button) findViewById(R.id.btnClear);
        btnBackspace = (Button) findViewById(R.id.btnBackspace);

        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edt2.getText().toString().equals("0"))
                    edt2.setText(edt2.getText().toString() + "0");
            }
        });
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt2.getText().toString().equals("0")) edt2.setText("1");
                else edt2.setText(edt2.getText().toString() + "1");
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt2.getText().toString().equals("0")) edt2.setText("2");
                else edt2.setText(edt2.getText().toString() + "2");
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt2.getText().toString().equals("0")) edt2.setText("3");
                else edt2.setText(edt2.getText().toString() + "3");
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt2.getText().toString().equals("0")) edt2.setText("4");
                else edt2.setText(edt2.getText().toString() + "4");
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt2.getText().toString().equals("0")) edt2.setText("5");
                else edt2.setText(edt2.getText().toString() + "5");
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt2.getText().toString().equals("0")) edt2.setText("6");
                else edt2.setText(edt2.getText().toString() + "6");
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt2.getText().toString().equals("0")) edt2.setText("7");
                else edt2.setText(edt2.getText().toString() + "7");
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt2.getText().toString().equals("0")) edt2.setText("8");
                else edt2.setText(edt2.getText().toString() + "8");
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt2.getText().toString().equals("0")) edt2.setText("9");
                else edt2.setText(edt2.getText().toString() + "9");
            }
        });
        btnDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edt2.setText(edt2.getText().toString() + ".");
            }
        });
        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt1.getText().toString().length() != 0 && edt2.getText().toString().length() == 0 && !edt1.getText().toString().equals("Math error!"))
                    edt2.setText(edt1.getText().toString() + "+");
                else
                    edt2.setText(edt2.getText().toString() + "+");
            }
        });
        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt1.getText().toString().length() != 0 && edt2.getText().toString().length() == 0 && !edt1.getText().toString().equals("Math error!"))
                    edt2.setText(edt1.getText().toString() + "-");
                else
                    edt2.setText(edt2.getText().toString() + "-");
            }
        });
        btnMultiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt1.getText().toString().length() != 0 && edt2.getText().toString().length() == 0 && !edt1.getText().toString().equals("Math error!"))
                    edt2.setText(edt1.getText().toString() + "×");
                else
                    edt2.setText(edt2.getText().toString() + "×");
            }
        });
        btnDivide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt1.getText().toString().length() != 0 && edt2.getText().toString().length() == 0 && !edt1.getText().toString().equals("Math error!"))
                    edt2.setText(edt1.getText().toString() + "÷");
                else
                    edt2.setText(edt2.getText().toString() + "÷");
            }
        });
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edt1.setText("");
                edt2.setText("0");
            }
        });
        btnBackspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt2.getText().toString().length() == 0) ;
                else if (edt2.getText().toString().length() == 1) edt2.setText("0");
                else edt2.setText(edt2.getText().toString().substring(0, edt2.getText().toString().length()-1));
            }
        });
        btnEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = edt2.getText().toString();

                String elementMath[] = null;
                InfixToPostfix  ITP = new InfixToPostfix();
                if (s.length()>0){
                    try{
                        if (!ITP.check_error) elementMath = ITP.processString(edt2.getText().toString());	//	tach bieu thuc thanh cac phan tu
                        if (!ITP.check_error) elementMath = ITP.postfix(elementMath);		// 	dua cac phan tu ve dang postfix
                        if (!ITP.check_error) edt1.setText(ITP.valueMath(elementMath)); 	//lay gia tri
                        edt2.setText("");
                    }catch(Exception e){
                        error();
                    }
                    if (ITP.check_error) error();
                }
            }
        });
    }
    public void error() {
        edt1.setText("Math error!");
        edt2.setText("");
    }
}
